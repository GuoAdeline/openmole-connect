resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.28")

addSbtPlugin("org.scalatra.sbt" % "sbt-scalatra" % "1.0.3")

addSbtPlugin("fr.iscpif" % "scalajs-execnpm" % "0.7")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.25")
